package flash.external;

extern class ExtensionContext extends flash.events.EventDispatcher {
  public function new();
  public function call(functionName:String, ?arg0:Dynamic = null, ?arg1:Dynamic = null, ?arg2:Dynamic = null, ?arg3:Dynamic = null, ?arg4:Dynamic = null, ?arg5:Dynamic = null):Dynamic;
  private function _call(functionName:String,arguments:Array<Dynamic>):Dynamic;

  public var actionScriptData(default, default):Dynamic;
  public function dispose():Void;
  function _disposed():Bool;
  function getActionScriptData():Dynamic;

  function setActionScriptData(object:Dynamic):Void;
  static public function createExtensionContext(extensionID:String,contextType:String):ExtensionContext;
  static private function _createExtensionContext(extensionID:String,contextType:String):ExtensionContext;
  static public function getExtensionDirectory(extensionID:String):flash.filesystem.File;
  static private function _getExtensionDirectory(extensionID:String):String;
}