package flash.net;

extern class NetworkInterface {
  public function new();
  public var name(default, default):String;
  public var displayName(default, default):String;
  public var addresses(default, default):Vector<flash.net.InterfaceAddress>;
  public var mtu(default, default):Int;
  public var hardwareAddress(default, default):String;
  public var parent(default, default):NetworkInterface;
  public var subInterfaces(default, default):Vector<flash.net.NetworkInterface>;
  public var active(default, default):Bool;
}