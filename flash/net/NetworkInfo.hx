package flash.net;

extern class NetworkInfo extends flash.events.EventDispatcher {
  public function new();
  private function onNetworkChange(event:flash.events.Event):Dynamic;
  public function findInterfaces():Vector<flash.net.NetworkInterface>;
  static public var isSupported(default, null):Bool;
  static public var networkInfo(default, null):NetworkInfo;
  static private function getInstance():NetworkInfo;
}