package flash.net;

extern class InterfaceAddress {
  public function new();
  public var address(default, default):String;
  public var broadcast(default, default):String;
  public var prefixLength(default, default):Int;
  public var ipVersion(default, default):String;
}
