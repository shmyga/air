package flash.events;

extern class LocationChangeEvent extends flash.events.Event {
  public function new(type:String,bubbles:Bool = false,cancelable:Bool = false,location:String = null);
  private var m_location:String;

  public override function clone():flash.events.Event;
  public override function toString():String;
  public var location(default, default):String;

  static public inline var LOCATION_CHANGE:String = "locationChange";
  static public inline var  LOCATION_CHANGING:String = "locationChanging";
}