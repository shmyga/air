package flash.media;

extern class StageWebView extends flash.events.EventDispatcher {
  public function new();

  private function init():Void;
  public function loadURL(url:String):Void;
  public function loadString(text:String,mimeType:String = "text/html"):Void;
  public function stop():Void;
  public function reload():Void;

  public var isHistoryBackEnabled(default, null):Bool;
  public function historyBack():Void;
  public var isHistoryForwardEnabled(default, null):Bool;
  public function historyForward():Void;
  public var location(default, null):String;

  public var title(default, null):String;
  public var viewPort(default, default):flash.geom.Rectangle;
  public var stage(default, default):flash.display.Stage;

  public function dispose():Void;
  public function assignFocus(direction:String = "none"):Void;

  public function drawViewPortToBitmapData(bitmap:flash.display.BitmapData):Void;
  static public var isSupported(default, null):Bool;
}